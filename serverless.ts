import type { AWS } from '@serverless/typescript';

import {hello, 
        // createOrderSpareParts, 
        // updateOrderSpareParts, deleteOrderSpareParts, 
        // getAllSparePartsByOrderId,
         createTechnicianWork,
        updateTechnicianWork, deleteTechnicianWork,
        createInvoice, addWorkEstimation, editWorkEstimation,
        getTechnicianWork,deleteInvoice,getCustomerInvoice,getCompanyInvoice, getInvoiceDetails,updateInvoice
      } from '@functions/index';

const serverlessConfiguration: AWS = {
  service: 'APIInvoiceManagement',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild','serverless-offline','serverless-dotenv-plugin','serverless-sequelize-migrations'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region:'eu-central-1',
    stage: "${opt:stage,'dev'}",
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      dialect:'mysql',
      DATABASE_NAME:process.env.DATABASE_NAME,
      DATABASE_USERNAME:process.env.DATABASE_USERNAME,
      DATABASE_PASSWORD:process.env.DATABASE_PASSWORD,
      DATABASE_HOST:process.env.DATABASE_HOST,
      DATABASE_PORT:process.env.DATABASE_PORT,
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
  },
  // import the function via paths
  functions: { hello, 
              // createOrderSpareParts, 
              // updateOrderSpareParts,
              // deleteOrderSpareParts, 
              // getAllSparePartsByOrderId, 
              updateInvoice,
              createTechnicianWork, 
              updateTechnicianWork, 
              deleteTechnicianWork,
              createInvoice, 
              addWorkEstimation,
              editWorkEstimation,
              getTechnicianWork,
              deleteInvoice,
              getCustomerInvoice,
              getCompanyInvoice,
              getInvoiceDetails
            },
  package: { individually: true , exclude:['pg-hstore'] },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
  },
};
// console.log("DATABASE_NAME",DATABASE_NAME)
module.exports = serverlessConfiguration;
