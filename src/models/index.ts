import { ANY } from '../shared/common';
import { 
  orderSpareParts,
  technicianWork,
  invoice,
  invoice_technicianWorkEstimation,
  users,
  customer,
  company,
  country,
  spareParts,
  city
} from '.';

export * from './orderSpareParts';
export * from './technicianWork';
export * from './invoice';
export * from './invoice_technicianWorkEstimation'
export * from './invoice_technicianWorkEstimation'
export * from './users'
export * from './customer'
export * from './company'
export * from './country'
export * from './spareParts'
export * from './city'




type ModelType = ANY;

export const models: ModelType = [
  orderSpareParts, technicianWork, invoice, invoice_technicianWorkEstimation,users, 
  customer, 
  company, country,spareParts,city
]