import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";
// import { invoice } from ".";

export interface technicianWorkI {
    id?: number;
    description?: string;
    timeSpent?: string;
    pricePerHour?: string;
    totalAmount?: string;
    technicianId?:number;
    orderId?:number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
    tax:string;
    type:string
}

@Table({
    modelName: 'technicianWork',
    tableName: 'technicianWork',
    timestamps: true,
    
})

export class technicianWork extends Model<technicianWorkI>{
    
    // @HasOne((): typeof invoice => invoice)
    // public invoice: typeof invoice;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.BIGINT)
    public technicianId: number;

    @Column(DataType.BIGINT)
    public orderId: number;

    @Column(DataType.TEXT)
    public description: string;

    @Column(DataType.TEXT)
    public timeSpent: string;

    @Column(DataType.TEXT)
    public pricePerHour: string;

    @Column(DataType.TEXT)
    public totalAmount: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.TEXT)
    public tax: string;

    @Column(DataType.STRING)
    public type: string;
}