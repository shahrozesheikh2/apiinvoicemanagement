import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { customer, company, users } from ".";


export interface invoiceI {
    id?: number;
    technicianId?: number;
    orderSparePartTotal?: number;
    technicianWorkTotal?: number;
    grandTotal:number
    orderId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
    customerId:number,
    companyId:number,
    billId:string,
    billStatusId:number,
    sparePartTax:number,
    servicesChargesTax:number;
    sparePartTotalWithTax:number;
    techWorkTotalWithTax:number;
    dueDate: Date;
    netTotal:number
    vatValue:number
    subTotal:number
    discount:number
    discountValue:number
    discountType:boolean
    billingAmount:number,
    additionalNotes:string
}

@Table({
    modelName: 'invoice',
    tableName: 'invoice',
    timestamps: true
})

export class invoice extends Model<invoiceI>{
    
    @BelongsTo((): typeof users => users)
    public users: typeof users; 

    @BelongsTo((): typeof company => company)
    public company: typeof company;

   
    @BelongsTo((): typeof customer => customer)
    public customer: typeof customer;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @ForeignKey((): typeof users => users)   
    @Column(DataType.NUMBER)
    public technicianId: number;

    @Column(DataType.NUMBER)
    public orderSparePartTotal: number;

    @Column(DataType.NUMBER)
    public sparePartTax: number;

    @Column(DataType.NUMBER)
    public servicesChargesTax: number;

    // @ForeignKey((): typeof technicianWork => technicianWork)
    @Column(DataType.NUMBER)
    public technicianWorkTotal: number;

    @Column(DataType.NUMBER)
    public sparePartTotalWithTax: number;

    // @ForeignKey((): typeof technicianWork => technicianWork)
    @Column(DataType.NUMBER)
    public techWorkTotalWithTax: number;

    @Column(DataType.NUMBER)
    public orderId: number;

    @Column(DataType.NUMBER)
    public grandTotal: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;
c
    @ForeignKey((): typeof customer => customer)
    @Column(DataType.NUMBER)
    public customerId: number;

    @ForeignKey((): typeof company => company)
    @Column(DataType.NUMBER)
    public companyId: number;

    @Column(DataType.STRING)
    public billId: string;

    @Column(DataType.STRING)
    public additionalNotes: string;

    @Column(DataType.INTEGER)
    public billStatusId: number;

    @Column({ type :DataType.VIRTUAL , 
        get() {return getBillStatus(this.getDataValue('billStatusId'));}})
        public billStatusValue : string

    @Column(DataType.DATE)
    public dueDate: Date;

    @Column(DataType.INTEGER)
    public netTotal: number;

    @Column(DataType.INTEGER)
    public vatValue: number;

    @Column(DataType.INTEGER)
    public subTotal: number;

    @Column(DataType.INTEGER)
    public discountValue: number;

    @Column(DataType.INTEGER)
    public discount: number;

    @Column(DataType.BOOLEAN)
    public discountType: boolean;

    @Column(DataType.INTEGER)
    public billingAmount: number;
}

const getBillStatus = (type:any) => {
    if (type === 0) return "pending"
    // if (type === 0) return "Private Person"
    if (type === 1) return "paid"
    if (type === 2) return "overdue"
    return ""
};