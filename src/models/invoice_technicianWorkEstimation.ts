import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";
// import { technicianWork } from ".";

export interface invoice_technicianWorkEstimationI {
    id?: number;
    orderId?: number;
    technicianId?: number;
    originalEstimationId?: string;
    correction?: string;
    total?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'invoice_technicianWorkEstimation',
    tableName: 'invoice_technicianWorkEstimation',
    timestamps: true
})

export class invoice_technicianWorkEstimation extends Model<invoice_technicianWorkEstimationI>{

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.BIGINT)
    public orderId: number;

    @Column(DataType.BIGINT)
    public technicianId: number;

    @Column(DataType.TEXT)
    public originalEstimationId: string

    @Column(DataType.TEXT)
    public correction: string

    @Column(DataType.TEXT)
    public total: string

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;
}