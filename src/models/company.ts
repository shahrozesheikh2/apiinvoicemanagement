import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table,} from "sequelize-typescript";
import { city, country } from ".";

export interface companyI{

  id?: number;
  logo?: JSON;
  companyName?: string;
  website: string;
  industryId?: number;
  noOfEmployees?: string;
  taxId?: number;
  allExpertise:JSON;
  address?: string;
  addressNotes?: string;
  countryId?: number;
  cityId?: number;
  stateId?: number;
  postalCode?: number;
  isBillingAddress?: boolean;
  billingAddress:string;
  billingAddressNotes:string;
  billingAddressCountryId:number;
  billingAddressCityId:number;
  billingAddressStateId:number;
  billingAddressPostalCode:number;
  bankName:string;
  accountTitle:string;
  iban?: string;
  bic?: string;
  vat?: number;
  legalInformation:JSON;
  payTermsDays:string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  createdBy?: number;
  updatedBy?: number;
  deltedBy?: number;
  contactInfo:number;
}

@Table({
  modelName: 'company',
  tableName: 'company',
  timestamps: true
})

export class company extends Model<companyI>{

  @BelongsTo((): typeof country => country,'billingAddressCountryId')
  public billingAddressCountry: typeof country;

  @BelongsTo((): typeof city => city,'billingAddressCityId')
  public billingAddressCity: typeof city;

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  public id: number;

  @Column(DataType.JSON)
  public logo: JSON;

  @Column(DataType.TEXT)
  public companyName: string;

  @Column(DataType.TEXT)
  public website: string;

  @Column(DataType.BIGINT)
  public industryId: number;

  @Column(DataType.TEXT)
  public noOfEmployees: string; 

  @Column(DataType.BIGINT)
  public taxId: number;

  @Column(DataType.JSON)
  public allExpertise: JSON;

  @Column(DataType.TEXT)
  public address: string;


  @Column(DataType.TEXT)
  public addressNotes: string;

 
  @Column(DataType.BIGINT)
  public countryId: number;

 
  @Column(DataType.BIGINT)
  public cityId: number;

  @Column(DataType.BIGINT)
  public stateId: number;

  @Column(DataType.BIGINT)
  public postalCode: number;

  @Column(DataType.TINYINT)
  public isBillingAddress: boolean;

  @Column(DataType.STRING)
  public billingAddress: string;

  @Column(DataType.STRING)
  public billingAddressNotes: string;

  @ForeignKey((): typeof country => country)
  @Column(DataType.INTEGER)
  public billingAddressCountryId: number;
  
  @ForeignKey((): typeof city => city)
  @Column(DataType.INTEGER)
  public billingAddressCityId: number;

  @Column(DataType.INTEGER)
  public billingAddressStateId: number;

  @Column(DataType.INTEGER)
  public billingAddressPostalCode: number;

  @Column(DataType.TEXT)
  public bankName: string;

  @Column(DataType.TEXT)
  public accountTitle: string;

  @Column(DataType.TEXT)
  public iban: string;

  @Column(DataType.TEXT)
  public bic: string;

  @Column(DataType.BIGINT)
  public vat: number;

  @Column(DataType.JSON)
  public legalInformation: JSON;

  @Column(DataType.INTEGER)
  public payTermsDays: number;

  @Column(DataType.JSON)
  public contactInfo: JSON;

  @Column(DataType.DATE)
  public createdAt: Date;

  @Column(DataType.INTEGER)
  public createdBy: number;

  @Column(DataType.DATE)
  public deletedAt: Date;

  @Column(DataType.INTEGER)
  public deletedBy: number;

  @Column(DataType.DATE)
  public updatedAt: Date;

  @Column(DataType.INTEGER)
  public updatedBy: number;
    
}
