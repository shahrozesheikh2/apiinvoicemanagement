import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { spareParts } from ".";


export interface orderSparePartsI {
    id?: number;
    orderId?: number;
    sparePartId?: number;
    quantity?: number;
    price?: number;
    description:string;
    comments:string;
    orderSparePartsStatusId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
    tax:number;
    type:string
}

@Table({
    modelName: 'orderSpareParts',
    tableName: 'orderSpareParts',
    timestamps: true
})

export class orderSpareParts extends Model<orderSparePartsI>{

    // @BelongsTo((): typeof order => order )
    // public order: typeof order;

    @BelongsTo((): typeof spareParts => spareParts )
    public spareParts: typeof spareParts;

    // @BelongsTo((): typeof orderSparePartsStatus => orderSparePartsStatus )
    // public orderSparePartsStatus: typeof orderSparePartsStatus;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    // @ForeignKey((): typeof order  =>  order )
    @Column(DataType.BIGINT)
    public orderId: number;

    @ForeignKey((): typeof  spareParts => spareParts  )
    @Column(DataType.BIGINT)
    public sparePartId: number;

    @Column(DataType.BIGINT)
    public quantity: number;

    @Column(DataType.BIGINT)
    public price: number;

    @Column(DataType.STRING)
    public description: string;

    @Column(DataType.STRING)
    public comments: string;

    @Column(DataType.STRING)
    public type: string;

    // @ForeignKey((): typeof orderSparePartsStatus  => orderSparePartsStatus )
    @Column(DataType.BIGINT)
    public orderSparePartsStatusId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.INTEGER)
    public tax: number;
}