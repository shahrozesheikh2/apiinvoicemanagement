import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const updateSchema =  Joi.object({
    id: Joi.number().integer().required(),
    description: Joi.string(),
    timeSpent: Joi.string().regex(/^([0-9]{2})\:([0-9]{2})\:([0-9]{2})$/),
    pricePerHour: Joi.number().integer(),
    companyId:Joi.number().integer()
  })

  // Joi.array().items(Joi.number())