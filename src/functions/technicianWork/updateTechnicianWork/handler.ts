import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { company, technicianWork } from '../../../models/index';
import schema, { updateSchema } from './schema';
import * as typings from '../../../shared/common';


const updateTechnicianWork:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await updateSchema.validateAsync(event.body)

    const{
      id,
      timeSpent,
      pricePerHour,
      companyId
    } = data

    const amountConversion = (Value: string) => {
      return  Value.toString().replace('.', ',')
    }

    const exist: typings.ANY = await technicianWork.findOne({where:{id , deletedAt:null}, attributes: {exclude: ['createdBy', 'updatedBy']}})

        if(!exist){
                  await sequelize.connectionManager.close();
                  return formatJSONResponse({
                  statusCode: 403,
                  message: `Id Invalid`,
                });
              } 
      let amountFloor       
      if(pricePerHour && timeSpent){

        const time = timeSpent;
        const amount = time.split(":").map((a: number,b: number) => a/(60**b)).reduce((a: any,b: any) => a+b) * pricePerHour;
        amountFloor = amount.toFixed(2)

      }else if (!pricePerHour && timeSpent){
        const price = exist.pricePerHour
        const time = timeSpent;
        
        const amount = time.split(":").map((a: number,b: number) =>  a/(60**b)).reduce((a: any,b: any) => a+b).toFixed(2) * price;
        amountFloor = amount.toFixed(2)

      }else if(pricePerHour && !timeSpent){
        const time = exist.timeSpent;
        const amount = time.split(":").map((a: number,b: number) => a/(60**b)).reduce((a: any,b: any) => a+b).toFixed(2) * pricePerHour;
        amountFloor = amount.toFixed(2)

      }
      const copmanyDetails = await company.findOne({
        where:{
          id:companyId
       },
       attributes:['vat']
     })
     // console.log("copmanyDetails",copmanyDetails)
      const tax = (JSON.parse(amountFloor) * copmanyDetails.vat)/100
     console.log("tax",tax)
  
    const dataObj: any = {
      totalAmount: amountConversion(amountFloor),
      tax:amountConversion(JSON.stringify(tax)),
      ...data,
      updatedAt: new Date
    }
    
     await technicianWork.update( dataObj, { where:{ id } } )

     const Response: typings.ANY = await technicianWork.findOne({where:{id , deletedAt:null}, attributes: {exclude: ['createdBy', 'updatedBy']}})
     
     await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully updated`,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(updateTechnicianWork);