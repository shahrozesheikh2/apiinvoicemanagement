import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { technicianWork } from '../../../models/index';
import schema, { getUserByIdSchema } from './schema';
import * as typings from '../../../shared/common';


const getTechnicianWork:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getUserByIdSchema.validateAsync(event.body)

    const {
      technicianId,
      orderId
    } = data

    
    const response : typings.ANY = await technicianWork.findAll({ where:{ technicianId, orderId ,deletedAt:null }, 
      attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']}
    })

    // if(!response.length){
    //   await sequelize.connectionManager.close();

    //   return formatJSONResponse({
    //     statusCode: 403,
    //     message: "User Not Exists Against Current Id"
    //   })
    // }
    await sequelize.connectionManager.close();
     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully Recieved`,
       response
     })
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
}
export const main = middyfy(getTechnicianWork);