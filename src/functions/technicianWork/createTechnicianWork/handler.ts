import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { company, technicianWork, technicianWorkI } from '../../../models/index';
import schema, { createSchema } from './schema';
import * as typings from '../../../shared/common';


const createTechnicianWork:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await createSchema.validateAsync(event.body)

    const amountConversion = (Value: string) => {
      return  Value.toString().replace('.', ',')
    }

    const{
      timeSpent,
      pricePerHour,
      companyId
    } = data

    const time = timeSpent
    const amount = time.split(":").map((a: number,b: number) => a/(60**b)).reduce((a: any,b: any) => a+b) * pricePerHour
    const amountFloor = amount.toFixed(2)
    // const amountConversion = amount.toString().replace('.', ',')
    const copmanyDetails = await company.findOne({
      where:{
        id:companyId
     },
     attributes:['vat']
   })
   console.log("copmanyDetails",copmanyDetails)
    let tax = (JSON.parse(amountFloor) * copmanyDetails.vat)/100


    const dataObj: any = {
      totalAmount: amountConversion(amountFloor),
      tax:amountConversion(JSON.stringify(tax)),
      type:'Hourly Rate',
      ...data,
    }
    
    const Response: technicianWorkI = await technicianWork.create( dataObj )
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully added`,
       Response
     })
  } 

  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(createTechnicianWork);