import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderSpareParts } from '../../../models/index';
import schema, { deleteSchema } from './schema';
import * as typings from '../../../shared/common';


const deleteOrderSpareParts:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await deleteSchema.validateAsync(event.body)

    const{
      id,
      ...other
    } = data

    const dataExist = await orderSpareParts.findOne({where:{id}})

    if(!dataExist){
    await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: `invalid ID`,
      });
    }
    
    const Response: typings.ANY = await orderSpareParts.update({ ...other, deletedAt: new Date }, {
      where:{
        id
      }
    })
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully deleted`,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(deleteOrderSpareParts);