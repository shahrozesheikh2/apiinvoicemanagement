import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderSpareParts } from '../../../models/index';
import schema, { getAllByIdSchema } from './schema';
import * as typings from '../../../shared/common';


const getAllSparePartsByOrderId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getAllByIdSchema.validateAsync(event.body)

    const{
      orderId
    } = data

    const dataExist = await orderSpareParts.findAll({where:{orderId}})

    if(!dataExist.length){
    await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: `invalid ID`,
      });
    }
    
    // const Response: typings.ANY = await orderSpareParts.findAll( { where:{ orderId }, limit:limit,
    //   offset: offset * limit, attributes: {exclude: ['createdBy', 'updatedBy', 'deletedBy']}
    // })

    const Response: typings.ANY = await orderSpareParts.findAll( { where:{ orderId },
       attributes: {exclude: ['createdBy', 'updatedBy', 'deletedBy']}
    })
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully Recieved `,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllSparePartsByOrderId);