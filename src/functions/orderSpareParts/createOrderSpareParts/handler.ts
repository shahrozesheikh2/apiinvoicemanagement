import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { orderSpareParts, orderSparePartsI } from '../../../models/index';
import schema, { createSchema } from './schema';
import * as typings from '../../../shared/common';


const createOrderSpareParts:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await createSchema.validateAsync(event.body)

    const Response: orderSparePartsI = await orderSpareParts.create( data )
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully added`,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(createOrderSpareParts);