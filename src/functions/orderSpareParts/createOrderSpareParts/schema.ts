import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const createSchema =  Joi.object({
    orderId: Joi.number().integer().required(),
    sparePartId: Joi.number().integer().required(),
    quantity: Joi.number().integer().required(),
    price: Joi.string().required(),
    orderSparePartsStatusId: Joi.number().integer().required(),
  })

  // Joi.array().items(Joi.number())