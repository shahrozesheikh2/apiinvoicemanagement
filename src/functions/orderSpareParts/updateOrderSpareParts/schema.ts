import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const updateSchema =  Joi.object({
    id: Joi.number().integer().required(),
    sparePartId: Joi.number().integer(),
    quantity: Joi.number().integer(),
    price: Joi.string(),
    orderSparePartsStatusId: Joi.number().integer(),
  })

  // Joi.array().items(Joi.number())