import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { invoice, invoiceI, orderSpareParts, technicianWork } from '../../../models/index';
import schema, { updateInvoiceSchema } from './schema';
import * as typings from '../../../shared/common';
// import axios from 'axios';

const updateInvoice:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
        
        sequelize.connectionManager.initPools();
        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await updateInvoiceSchema.validateAsync(event.body)

    const{
        technicianId,
        orderId,
        discount,
        discountValue,
        discountType,
        additionalNotes
    } = data

    const existInvoice = await invoice.findOne({where:{orderId ,deletedAt:null}})

    if(!existInvoice){
      return formatJSONResponse({
        statusCode: 403,
         message: `Invoice Not Found`
       });
    }
    
    let workData = await technicianWork.findAll({ where:{ technicianId ,orderId,deletedAt:null },
        attributes: ['id', 'technicianId', 'orderId' ,'totalAmount','tax']
    })

    workData = JSON.parse(JSON.stringify(workData))

    let workSum = 0;
    let servicesChargesTax = 0;

    const amountConversionDot = (Value: string) => {
      return  Value.toString().replace(',', '.')
    }

    const amountConversionCom = (Value: string) => {
      return  Value.toString().replace('.', ',') 
    }

    for(let i of workData){
        console.log("i.totalAmount",i.totalAmount);
        const value = amountConversionDot(i.totalAmount)
        console.log("i.tax",i.tax);
        const tax =  amountConversionDot(i.tax)
        console.log("tax",parseFloat(tax));
        workSum = workSum + parseInt(value)
        servicesChargesTax = servicesChargesTax + parseFloat(tax)
    }

    let orderSparePartlist: any ;

      const spartParts: typings.ANY = await orderSpareParts.findAll({where:{
        orderId,updatedBy:technicianId,orderSparePartsStatusId:6
      }})
      // await axios.post(`${process.env.ORDER_MANAGEMENT}getAllSparePartsByOrderId`, {orderId,technicianId,orderSparePartsStatusId:6});


    orderSparePartlist = JSON.parse(JSON.stringify(spartParts))

    let sparePartSum = 0;
    let sparePartTax = 0
    for (let s of orderSparePartlist){
      console.log("s.totalAmount",s.price);
      const value = amountConversionDot(s.price)
      sparePartSum = sparePartSum + parseInt(value)

      console.log("s.totalAmount",s.tax);
      const tax = amountConversionDot(s.tax)
      sparePartTax = sparePartTax + parseInt(tax)

    }
    console.log("sparePartSum",sparePartSum)

   
    // const billStatusId = 1 // for paid


    const netTotal = sparePartSum + workSum


    let totalTax = sparePartTax + servicesChargesTax
    const vatValue = totalTax.toFixed(2)
    console.log("vatValue",vatValue)

    let subTotal = netTotal + parseInt(vatValue)

    let billingAmount = subTotal
    
    if(discountValue){
      billingAmount = subTotal - parseInt(discountValue)
    }

    const dataObj: any = {
      netTotal,
      vatValue:amountConversionCom(vatValue),
      subTotal:amountConversionCom(JSON.stringify(subTotal)),
      billingAmount:amountConversionCom(JSON.stringify(billingAmount)),
      discount,
      discountValue,
      discountType,
      additionalNotes
    }
    console.log("dataObj",dataObj)
    // sequelize.connectionManager.initPools();

    await invoice.update({...dataObj},{where:{orderId,technicianId}})
    // console.log ("response",Response)
    // await sequelize.connectionManager.close();
    const response = invoice.findOne({where:{orderId,technicianId}})
     return formatJSONResponse({
      statusCode: 200,
       message: `Successfully Updated`,
       response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(updateInvoice);