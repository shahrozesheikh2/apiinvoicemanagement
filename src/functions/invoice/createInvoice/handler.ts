import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { company, invoice, invoiceI, orderSpareParts, technicianWork } from '../../../models/index';
import schema, { createSchema } from './schema';
import * as typings from '../../../shared/common';
// import axios from 'axios';

const createInvoice:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
        
        sequelize.connectionManager.initPools();
        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await createSchema.validateAsync(event.body)

    const{
        technicianId,
        orderId,
        customerId,
        companyId,
        discount,
        discountValue,
        additionalNotes,
        discountType
    } = data

    const existInvoice = await invoice.findOne({where:{orderId ,deletedAt:null}})

    if(existInvoice){
      return formatJSONResponse({
        statusCode: 403,
         message: `Invoice Against This order Already Generated`
       });
    }
    
    let workData = await technicianWork.findAll({ where:{ technicianId ,orderId,deletedAt:null },
        attributes: ['id', 'technicianId', 'orderId' ,'totalAmount','tax']
    })

    workData = JSON.parse(JSON.stringify(workData))

    let workSum = 0;
    let servicesChargesTax = 0;

    // const amountConversion = (Value: string) => {
    //   return  Value.toString().replace('.', ',')
    // }

    const amountConversionDot = (Value: string) => {
      return  Value.toString().replace(',', '.')
    }

    const amountConversionCom = (Value: string) => {
      return  Value.toString().replace('.', ',') 
    }

    for(let i of workData){
        console.log("i.totalAmount",i.totalAmount);
        const value = amountConversionDot(i.totalAmount)
        console.log("i.tax",i.tax);
        const tax =  amountConversionDot(i.tax)
        console.log("tax",parseFloat(tax));
        workSum = workSum + parseInt(value)
        servicesChargesTax = servicesChargesTax + parseFloat(tax)
    }

    let orderSparePartlist: any ;

      const spartParts: typings.ANY = await orderSpareParts.findAll({where:{
        orderId,updatedBy:technicianId,orderSparePartsStatusId:6
      }})
      // await axios.post(`${process.env.ORDER_MANAGEMENT}getAllSparePartsByOrderId`, {orderId,technicianId,orderSparePartsStatusId:6});


      orderSparePartlist = JSON.parse(JSON.stringify(spartParts))

      // console.log("orderSparePartlist",orderSparePartlist)

      
    let sparePartSum = 0;
    let sparePartTax = 0
    for (let s of orderSparePartlist){
      console.log("s.totalAmount",s.price);
      const value = amountConversionDot(s.price)
      sparePartSum = sparePartSum + parseInt(value)

      console.log("s.tax",s.tax);
      const tax = amountConversionDot(s.tax)
      sparePartTax = sparePartTax + parseInt(tax)

    }
    console.log("sparePartSum",sparePartSum)
    console.log("workSum",workSum)



   

    // let worksum =  workData.map(i =>{
    //   console.log("iii",i);
    
      
    //   return i
    // })

    // let sum = invoiceData ? invoiceData.reduce((acc, item) => { return acc + item.totalAmount; }, 0) : 0;

    const number  = parseInt(`${Math.random() * 1000000}`)

    const billId = `FF-BE1-${number}`

    const billStatusId = 1 // for paid

    const sparePartTotalWithTax = sparePartSum +sparePartTax
    const techWorkTotalWithTax = workSum + servicesChargesTax

    console.log("sparePartTotalWithTax",sparePartTotalWithTax)
    console.log("techWorkTotalWithTax",techWorkTotalWithTax)

    

    const netTotal = sparePartSum + workSum

    console.log("netTotal",netTotal)

    console.log("discountValue",discountValue)
    let subTotal = netTotal;
    if(discountValue){
      subTotal = netTotal - parseInt(discountValue)
    }


    let totalTax = sparePartTax + servicesChargesTax
    const vatValue = totalTax.toFixed(2)
    console.log("vatValue",vatValue)

    let billingAmount = subTotal + parseInt(vatValue)

    console.log("subTotal",subTotal)

    console.log("billingAmount",billingAmount)

    const total = sparePartTotalWithTax + techWorkTotalWithTax

    const companyDetails = await company.findOne({where:{id:companyId}})

    const dueDate = new Date( Date.now() + companyDetails.payTermsDays * 24 * 60 * 60 * 1000)

    
    const dataObj: any = {
      technicianId,
      orderId,
      // grandTotal:total,
      // orderSparePartTotal:sparePartSum,
      // technicianWorkTotal: workSum,
      customerId,
      companyId,
      billId,
      billStatusId,
      // sparePartTax,
      // servicesChargesTax,
      // sparePartTotalWithTax,
      // techWorkTotalWithTax,
      netTotal,
      vatValue:amountConversionCom(vatValue),
      subTotal:amountConversionCom(JSON.stringify(subTotal)),
      billingAmount:amountConversionCom(JSON.stringify(billingAmount)),
      discount,
      discountValue,
      additionalNotes,
      discountType,
      dueDate
    }

    const response: invoiceI = await invoice.create( dataObj )
    // console.log ("response",Response)
    // await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
       message: `Successfully added`,
       response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(createInvoice);