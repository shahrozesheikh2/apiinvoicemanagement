import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const createSchema =  Joi.object({
    orderId:Joi.number().integer().required(),
    technicianId: Joi.number().integer().required(),
    customerId:Joi.number().integer().required(),
    companyId:Joi.number().integer().required(),
    discount:Joi.number().integer(),
    discountType:Joi.boolean(),// 0--> percent , 1---->fixedAmount
    discountValue:Joi.string(),
    additionalNotes:Joi.string()
    // orderSparePartTotal: Joi.number().integer().required(),
    // technicianWorkTotal: Joi.number().integer().required(),
  })

  // Joi.array().items(Joi.number())