import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const companyInvoiceSchema =  Joi.object({
    id:Joi.number().integer(),
    orderId:Joi.number().integer(),
    technicianId:Joi.number().integer()
  })

  // Joi.array().items(Joi.number())