import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { company, customer, invoice, orderSpareParts, spareParts, technicianWork, users } from '../../../models/index';
import schema, { companyInvoiceSchema } from './schema';
import * as typings from '../../../shared/common';
import { country } from 'src/models/country';
import { city } from 'src/models/city';
import AWS from "aws-sdk";

const getInvoiceDetails:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await companyInvoiceSchema.validateAsync(event.body)

    const{
      id,
      orderId,
      technicianId
    } = data

    let  whereClause: { [key: string]: typings.ANY }  = {}

    if (id) {
      whereClause.id =  id;
     
    }

    if (orderId && technicianId ) {
      whereClause.orderId =  orderId;
      whereClause.technicianId =  technicianId;
    }
    const invoiceDetails = await invoice.findOne(
      {
        where:{ ...whereClause, deletedAt:null},
        attributes:{exclude:['createdBy','deletedAt','updatedAt','updatedBy','deletedBy']},
        include:[
          {
            as: 'users',
            model: users,
            attributes: ['firstName','lastName'],
          },
          {
            as: 'company',
            model: company,
            include:[
            {
              as: 'billingAddressCountry',
              model: country,
              attributes:['id','name']
            },
            {
              as: 'billingAddressCity',
              model: city,
              attributes:['id','name']
            },
          ]
            // attributes: ['companyName','address', 'legalInformation','logo','sparePartTax','serviceChargesTax']
          },
          {
            as: 'customer',
            model: customer,
            attributes: ['firstName','companyName', 'lastName', 'email', 'billingAddress','billingAddressPostalCode','mobileNumberExtension','mobileNumber','taxId'],
             include:[
            {
              as: 'country',
              model: country,
              attributes:['id','name']
            },
            {
              as: 'city',
              model: city,
              attributes:['id','name']
            }
          ]
          }
         
        ],
    })

    if(invoiceDetails == null || !invoiceDetails){
       return formatJSONResponse({
      statusCode: 200,
      message: `Invoice Not Found`,
    });
    }
    

     const data1 =  await getImage(invoiceDetails)

    let buf = Buffer.from(data1.Body);
    let base64 = buf.toString('base64');
    console.log("base64",base64)

    if(!invoiceDetails){
      throw Error ('Invoice Note Found')
    }
  
    // console.log("orderSpareParts",invoiceDetails)
  let  spartPartsData: typings.ANY = await orderSpareParts.findAll({
    where:{
    orderId:invoiceDetails.orderId,updatedBy:invoiceDetails.technicianId,orderSparePartsStatusId:6,
    },
    include:[{
      as:'spareParts',
      model: spareParts,
      attributes: {exclude: ['updatedAt','updatedBy','deletedBy']},
    }]
  })

  let technicianWorkData : typings.ANY = await technicianWork.findAll({ where:{ technicianId:invoiceDetails.technicianId, orderId:invoiceDetails.orderId ,deletedAt:null }, 
    attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']}
  })

  spartPartsData = JSON.parse(JSON.stringify(spartPartsData))
  console.log("spartPartsData",spartPartsData)
  let spartPartsDetails = []

  for(let arr of spartPartsData){
    let obj = {
      itemName:arr.spareParts.itemName,
      code:arr.spareParts.code,
      comments:arr.comments,
      quantity:arr.quantity,
      rate:arr.spareParts.price,
      totalPrice:arr.price,
      tax:arr.tax,
      type:arr.type
    }
    console.log("obj",obj)
    spartPartsDetails.push(obj)
  }


    const Response = {
      invoiceDetails,
      spartPartsDetails,
      technicianWorkData
    }

    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully Recieved`,
       Response,
       base64
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getInvoiceDetails);

async function getImage(invoiceDetails){
  
  const s3 = new AWS.S3({
      accessKeyId: process.env.A_ACCESS_KEY_ID,
      secretAccessKey: process.env.A_SECRET_ACCESS_KEY,
      // region: process.env.region
  });
  const data :any =  s3.getObject(
    {
        Bucket: process.env.FILE_UPLOAD_BUCKET_NAME,
        Key: invoiceDetails.company?.logo.awsKey
      }
    
  ).promise();

  return data;
}