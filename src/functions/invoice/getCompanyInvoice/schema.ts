import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const companyInvoiceSchema =  Joi.object({
    companyId:Joi.number().integer().required(),
    offset: Joi.number().min(0).default(0),
    limit: Joi.number().max(999).default(10),
    filters:{
        billStatusId:Joi.number().integer(),
        billId:Joi.string(),
        requestDate:Joi.string(),
        technicianFirstName:Joi.string(), 
        technicianLastName:Joi.string(), 
        customerName : Joi.string(),
        customerType:Joi.number().integer()

    }
  })

  // Joi.array().items(Joi.number())