import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { customer, invoice, users } from '../../../models/index';
import schema, { companyInvoiceSchema } from './schema';
import * as typings from '../../../shared/common';

import { Op } from 'sequelize';

const getCompanyInvoice:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await companyInvoiceSchema.validateAsync(event.body)

    const{
      companyId,
      limit,
      offset,
      filters
    } = data

    let  whereClause: { [key: string]: typings.ANY }  = {}
    let  whereClauseUser: { [key: string]: typings.ANY }  = {}
    let  whereClauseCustomer: { [key: string]: typings.ANY }  = {}


    console.log("filters",filters?.billStatusId)
    if (filters?.billStatusId != null) {
      whereClause.billStatusId =  filters.billStatusId;
     
    }

    if (filters?.billId != null) {
      whereClause.billId =  filters.billId;
      // console.log("whereClause",whereClause)
    }

    if (filters?.requestDate != null) {
      whereClause.createdAt =  {  [Op.startsWith]: filters.requestDate };
      // console.log("whereClause",whereClause)
    }

    console.log("whereClause",whereClause)

    if (filters?.technicianName != null) {
      whereClauseUser.firstName =  {  [Op.startsWith]: filters.technicianName }; 
      // console.log("whereClause",whereClause)
    }

    if (filters?.technicianLastName != null) {
      whereClauseUser.lastName =  {  [Op.startsWith]: filters.technicianLastName }; 
      // console.log("whereClause",whereClause)
    }


    console.log("whereClauseUser",whereClauseUser)

    if (filters?.customerName != null) {
      whereClauseCustomer.firstName =  {  [Op.startsWith]: filters.customerName }; 
      // console.log("whereClause",whereClause)
    }

    if (filters?.customerType != null) {
      whereClauseCustomer.customerType =  {  [Op.startsWith]: filters.customerType }; 
      // console.log("whereClause",whereClause)
    }

    if (filters?.customerTags != null) {
      whereClauseCustomer.customerTags =  {  [Op.startsWith]: filters.customerTags }; 
      // console.log("whereClause",whereClause)
    }

    console.log("whereClauseCustomer",whereClauseCustomer)




    const Response = await invoice.findAndCountAll(
      {
        where:{companyId,deletedAt:null,...whereClause},
        limit:limit,
        offset: offset * limit,
        attributes:{exclude:['createdBy','deletedAt','updatedAt','updatedBy','deletedBy']},
        include:[{
              as: 'users',
              where:{...whereClauseUser},
              model: users,
              attributes: ['id', 'firstName','lastName'],
        },{
          as: 'customer',
          where:{...whereClauseCustomer},
          model: customer,
          attributes: ['id', 'firstName','lastName'],
        }]
      })

    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully Recieved`,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getCompanyInvoice);