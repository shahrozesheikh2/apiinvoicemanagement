import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { invoice, users } from '../../../models/index';
import schema, { customerInvoiceSchema } from './schema';
import * as typings from '../../../shared/common';


const getCustomerInvoice:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
    sequelize.authenticate()
    const data :typings.ANY   = await customerInvoiceSchema.validateAsync(event.body)

    const{
      customerId,
      limit,
      offset
    } = data

    const Response = await invoice.findAndCountAll(
      {
        where:{customerId , deletedAt:null},
        limit:limit,
        offset: offset * limit,
        attributes:{exclude:['createdBy','deletedAt','updatedAt','updatedBy','deletedBy']},
        include:{
              as: 'users',
              model: users,
              attributes: ['id', 'firstName','lastName'],
        }
      })

    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message: `Successfully Recieved`,
       Response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getCustomerInvoice);