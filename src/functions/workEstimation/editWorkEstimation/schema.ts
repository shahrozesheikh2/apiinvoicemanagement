import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const createSchema =  Joi.object({
    id: Joi.number().integer().required(),
    correction: Joi.string().regex(/^([0-9]{2})\:([0-9]{2})$/).required(),
    condition: Joi.string().valid('add', 'sub').required()
  })
