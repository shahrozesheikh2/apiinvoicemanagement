import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { invoice_technicianWorkEstimation } from '../../../models/index';
import schema, { createSchema } from './schema';
import { add, sub, str } from 'timelite/time'
import * as typings from '../../../shared/common';


const editWorkEstimation:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await createSchema.validateAsync(event.body)

    const {
      id,
      correction,
      condition
    } = data


    const dataExist = await invoice_technicianWorkEstimation.findOne({where:{id}})

    if(!dataExist){
     await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 403,
        message: `invalid ID`,
      });
    }

    if(condition === 'add'){
      const result =  str(add([dataExist.originalEstimationId, correction]))
      
      const obj = {
        correction: correction,
        total: result
      }

      const Response = await invoice_technicianWorkEstimation.update( obj, {where: {id}} )
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 200,
        message: `Successfully edit correction`,
        Response
     })

    } 
    if(condition === 'sub') {
      const result =  str(sub([dataExist.originalEstimationId, correction])) 

      const obj = {
        correction: correction,
        total: result
      }

      const Response = await invoice_technicianWorkEstimation.update( obj, {where: {id}} )
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 200,
        message: `Successfully edit correction`,
        Response
     })
    } 

  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(editWorkEstimation);