import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;


export const createSchema =  Joi.object({
    orderId:Joi.number().integer().required(),
    technicianId: Joi.number().integer().required(),
    originalEstimationId: Joi.string().regex(/^([0-9]{2})\:([0-9]{2})$/).required()
  })
